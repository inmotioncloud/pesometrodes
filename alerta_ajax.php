<?php
require_once "controller/alertaController.php";
require_once "controller/class.inputfilter_clean.php";

$sanitizer = new InputFilter();
foreach($_POST as $key=>$val){
	$arr[$key] =  $sanitizer->process($val);
}

$list = new alertaController();

switch(intval($arr['opAcc'])){
	case 1:		
		// obtiene listado de datos de alertas.
		$list->getData();
		break;	
	case 2:
		// graba opcion de alerta en BDD.
		$list->setMotivoAlerta($arr);		
		break;
	default:
		exit();
		break;
}

?>