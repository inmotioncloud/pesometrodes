<?php
class helper{
    public function url(){
		
		$pos = strpos($_SERVER['PHP_SELF'], 'pesometrodes');				
		if ($pos === false) {
			$ruta = "/";
		} else {
			$ruta = "/pesometrodes/";
		}
		
        return sprintf(
            "%s://%s%s",
            isset($_SERVER['HTTPS']) && $_SERVER['HTTPS'] != 'off' ? 'https' : 'http',
            $_SERVER['SERVER_NAME'],
            $ruta.'mtManifiesto_add.php?id='

        );
}
}

?>