<?php
require_once "model/correaModel.php";
require_once "controller/class.inputfilter_clean.php";

class correaController{
	private $dd;
	private $sanitizer;
	
	function correaController(){
		$this->dd = new correaModel();		
		$this->sanitizer = new InputFilter();				
	} 	
	
	public function getData(){
		// muestra JSON con alertas para llenar el datatables
		return $this->dd->getData();
	}	
	
	public function resetCorrea(){
		
		if($this->dd->resetCorrea()){			
			echo json_encode(array("result"=>"1","mje"=>"El contador de horas fue reseteado exitosamente."));					
		} else {
			echo json_encode(array("result"=>"0","mje"=>"Ubo un error en el proceso, favor de volver a intentar."));
		}
			
	
	}
}


?>