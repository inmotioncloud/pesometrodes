<!DOCTYPE html>

<html lang="en">

  <head>
	<?php require_once "meta.php";?>
	<style>
		.nav-pills .nav-link.active, .nav-pills .show > .nav-link {
			color: #fff !important;
			background-color: #ccc;
		}
		.row-activo{
			background-color: red !important;
		}
	</style>
  </head>

  <body>

	<?php require_once "menu.php";?>

    <!-- Page Content -->
    <div class="container">

      <!-- Page Heading/Breadcrumbs -->
      <h1 class="mt-4 mb-3"><i class="far fa-list-alt"></i> Manifiestos</h1>

      <ol class="breadcrumb">
        <li class="breadcrumb-item">Home</li>
        <li class="breadcrumb-item active">Manifiesto</li>
      </ol>

      <div class="row">

        <!-- Blog Entries Column -->
        <div class="col-md-12">

          <!-- Blog Post -->
          <div class="card mb-4">        
            <div class="card-body">
			
			<button id="btnNuevoManifiesto" name="btnNuevoManifiesto" type="button" class="btn btn-primary btn-sm"><i class="far fa-plus-square"></i> Crear nuevo manifiesto</button> 
			<br>
			<br>
			<table id="tblManifiestos" class="table table-striped table-bordered display nowrap" style="width:100%">
				<thead>
					<tr>
						<th>Manifiesto</th>							
						<th>Puerto Origen</th>
						<th>Puerto Destino</th>
						<th>Fecha inicio</th>
						<th class="text-center">Acciones</th>
						</tr>
					</thead>		
									
				</table>

				</div>
				<div class="card-footer text-muted"></div>
			</div>
		</div>
	</div>
</div>
      <!-- /.row -->

    </div>
	<!-- Modal -->

	<!-- Modal -->
	<div class="modal fade" id="modalEliminar" name="modalEliminar" tabindex="-1" role="dialog" aria-labelledby="exampleModalLongTitle" aria-hidden="true">
	  <div class="modal-dialog modal-dialog-centered" role="document">
		<div class="modal-content">
		  <div class="modal-header">
			<h5 class="modal-title" id="exampleModalLongTitle">Eliminar Manifiesto</h5>
			<button type="button" class="close" data-dismiss="modal" aria-label="Close">
			  <span aria-hidden="true">&times;</span>
			</button>
		  </div>
		  <div class="modal-body">
		  
		  	<form id="frmIngresoMotivo">				
				<div id="txtIngresoOtro">					
					<div id="mjeAdvertencia" class="alert alert-warning"><strong>Advertencia!</strong> <p>Al eliminar este manifiesto, tambien se eliminaran las alertas asociadas.</p></div>
					<p><center><strong>¿Está seguro de querer eliminar este Manifiesto?.</strong></center></p>
				</div>
			</form>
			
		  </div>
		  <div class="modal-footer">
			<button type="button" class="btn btn-secondary" data-dismiss="modal"><i class="fas fa-ban"></i> Cancelar</button>
			<button type="button" class="btn btn-primary btn-danger" id="btnEliminarManifiesto"><i class="far fa-trash-alt"></i> Eliminar Manifiesto</button>
		  </div>
		</div>
	  </div>
	</div>

    <div class="modal fade" id="modalActivarManifiesto" name="modalActivarManifiesto" tabindex="-1" role="dialog" aria-labelledby="exampleModalLongTitle" aria-hidden="true">
	  <div class="modal-dialog modal-dialog-centered" role="document">
		<div class="modal-content">
		  <div class="modal-header">
			<h5 class="modal-title" id="exampleModalLongTitle">Activación de Manifiesto</h5>
			<button type="button" class="close" data-dismiss="modal" aria-label="Close">
			  <span aria-hidden="true">&times;</span>
			</button>
		  </div>
		  <div class="modal-body">
		  	<div id="txtIngresoOtro">				
				<div id="mjeAdvertencia" class="alert alert-warning"><strong>Advertencia!</strong> <p>¿Esta seguro de querer activar este manifiesto?</p></div>
			</div>
		  </div>
		  <div class="modal-footer">
			<button type="button" class="btn btn-secondary" data-dismiss="modal"><i class="fas fa-ban"></i> Cancelar</button>			
			<button id="btnActivarManifiesto" type="button" class="btn btn-success" data-dismiss="modal"><i class="far fa-check-circle"></i> Activar</button>			
		  </div>
		</div>
	  </div>
	</div>


	<?php 
		require_once "footer.php";
		require_once "js.php";
	?>

	<script>
		var idMan = 0;
		$(document).ready(function(){
			$('#btnNuevoManifiesto').click(function(){
				location.href='mtManifiesto_add.php';
			});
			
			$('#tblManifiestos').DataTable({			
					
					"ajax": {
						"url": 'manifiesto_ajax.php',
						"type": 'POST',
						"data": {"opAcc": 4}
						},
						"language": {
							"sProcessing":     "Procesando...",
							"sLengthMenu":     "Mostrar _MENU_ registros",
							"sZeroRecords":    "No se encontraron resultados",
							"sEmptyTable":     "Ningún dato disponible en esta tabla",
							"sInfo":           "Mostrando _START_ de _END_ de _TOTAL_ registros",
							"sInfoEmpty":      "Mostrando 0 de 0 de 0 registros",
							"sInfoFiltered":   "(filtrado de un total de _MAX_ registros)",
							"sInfoPostFix":    "",
							"sSearch":         "Buscar:",
							"sUrl":            "",
							"sInfoThousands":  ",",
							"sLoadingRecords": "Cargando...",
							"oPaginate": {
								"sFirst":    "Primero",
								"sLast":     "Último",
								"sNext":     "Siguiente",
								"sPrevious": "Anterior"
							},
							"oAria": {
								"sSortAscending":  ": Activar para ordenar la columna de manera ascendente",
								"sSortDescending": ": Activar para ordenar la columna de manera descendente"
							}
						},
						dataSrc: 'data',
						columns: [
							{ data: 'id' },				
							{ data: 'puerto_origen' },
							{ data: 'puerto_destino' },
							{ data: 'date' },				
							{ data: 'acciones'  }
						],
						order: [[ 3, "desc" ]],
						columnDefs: [			
							{ "className": "dt-center", "targets": "_all" }
						],
						responsive: true						
						
			});

			$("#btnEliminarManifiesto").on("click", function(){

				$.ajax({
					url: "manifiesto_ajax.php",
					type: "POST",			
					data: "&opAcc=1&manif=" + idMan ,
					beforeSend : function (){
						HoldOn.open({theme:'sk-rect', message:"<h4>Cargando...</h4>"});
					},
					success: function(response){						
						var data = JSON.parse(response);
				
						if(data.result==1){							
							$('#modalEliminar').modal('hide');
							bootbox.alert({
								message: data.mje,
								callback: function () {
									$('#tblManifiestos').DataTable().ajax.reload();						
								}
							});						
							HoldOn.close();						
						} else {
							bootbox.alert(data.mje);
							HoldOn.close();
						}
					}
				});
			});

			$("#btnActivarManifiesto").on("click", function(){
				
				$.ajax({
					url: "manifiesto_ajax.php",
					type: "POST",			
					data: "&opAcc=3&manif=" + idMan ,
					beforeSend : function (){
						HoldOn.open({theme:'sk-rect', message:"<h4>Cargando...</h4>"});
					},
					success: function(response){
						
						var data = JSON.parse(response);
						
						if(data.result==1){							
							$('#modalActivarManifiesto').modal('hide');
							bootbox.alert({
								message: data.mje,
								callback: function () {
									$('#tblManifiestos').DataTable().ajax.reload();						
								}
							});						
							HoldOn.close();						
						} else {
							bootbox.alert(data.mje);
							HoldOn.close();
						}
					}
				});
			});
			
			jQuery.extend( jQuery.fn.dataTableExt.oSort, {
				"date-uk-pre": function ( a ) {
					var ukDatea = a.split('/');
					return (ukDatea[2] + ukDatea[1] + ukDatea[0]) * 1;
				},

				"date-uk-asc": function ( a, b ) {
					return ((a < b) ? -1 : ((a > b) ? 1 : 0));
				},

				"date-uk-desc": function ( a, b ) {
					return ((a < b) ? 1 : ((a > b) ? -1 : 0));
				}
			});
			
		});

		function openModal(idManifiesto, op){	
			idMan = idManifiesto;
			
			$("#idManifiesto").val(idManifiesto);			
			switch(op){
				case 1:
					// limpiar formulario de motivo.
					$('#modalEliminarManifiesto').modal('show'); 
					break;
				case 2:
					$('#modalActivarManifiesto').modal('show'); 
					break;
			}
		}	
			
	</script>

  </body>

</html>
