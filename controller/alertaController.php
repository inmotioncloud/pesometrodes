<?php
require_once "model/alertaModel.php";
require_once "controller/class.inputfilter_clean.php";

class alertaController{
	private $dd;
	private $sanitizer;
	
	function alertaController(){
		$this->dd = new alertaModel();		
		$this->sanitizer = new InputFilter();
	} 
	
	public function getListaMotivo(){
		// muestra listado de motivos a seleccionar para motivo alertas.		
		$array = json_decode($this->dd->getListMotivo(), True);
		
		echo '<option id="0" value="0">-- Seleccione motivo --</option>';
		foreach ($array as $key=>$val) {
			echo '<option id="'.$val['cod_alerta'].'" value="'.$val['cod_alerta'].'">'.($val['motivo']).'</option>';			
		}
		
	}
	
	public function getData(){
		// muestra JSON con alertas para llenar el datatables
		echo $this->dd->getData();
	}

	public function setMotivoAlerta($array){		
		
		foreach($array as $key=>$val){
			$arr[$key] =  $this->sanitizer->process($val);
		}
		
		if(intval($arr['idAlerta'])!=0 && $this->dd->existeAlerta(intval($arr['idAlerta']))){	
		
			if($this->dd->existeMotivo($arr['opMotivo'])){
				
				if(intval($arr['opMotivo']) == 8){ // Si el motivo es 8 (cambio de bodega), activa la siguiente bodega.
					//activar bodega siguiente
					$arrBodegas = json_decode($this->dd->setActivarSiguienteBodega());

					if($arrBodegas->bodegaSiguiente==0 && $arrBodegas->bodegaActual==0){
						echo json_encode(array("result"=>"0","mje"=>"No es posible hacer un cambio de Hold, no existe una bodega activada."));
						exit();
					} else if($arrBodegas->bodegaSiguiente==0 && $arrBodegas->bodegaActual>0){
						$mje1 = "<br><strong> Hold ".$arrBodegas->bodegaActual." es la ultima bodega.</strong>";
						$mje2 = " Hold ".$arrBodegas->bodegaActual." es la ultima bodega";
					} else {
						$mje1 = "<br><strong>Se cambio Hold ".$arrBodegas->bodegaActual." a Hold ".$arrBodegas->bodegaSiguiente.".</strong>";
						$mje2 = " de Hold ".$arrBodegas->bodegaActual." a Hold ".$arrBodegas->bodegaSiguiente;
					}
					$motivo = utf8_encode(strip_tags($this->dd->txtMotivo) . $mje2);
					
				} else if(intval($arr['opMotivo']) == 9){ // Si el motivo seleccionado es 9 agrega el texto ingresado por el usuario.					
					$motivo = utf8_encode($arr['txtMotivo']);
					
				} else {
					$motivo = utf8_encode(strip_tags($this->dd->txtMotivo));					
				}
				
				// Envia los datos para guardar en bdd.
				if($this->dd->setMotivoAlerta(intval($arr['opMotivo']), $motivo, intval($arr['idAlerta']))){
					echo json_encode(array("result"=>"1","mje"=>"El registro fue guardado exitosamente.".$mje1));
				} else {
					echo json_encode(array("result"=>"0","mje"=>"Ubo un error al grabar los datos, favor de volver a intentar."));
				}
			
			} else {
				echo json_encode(array("result"=>"0","mje"=>"El motivo seleccioando no es válido, favor de volver a intentar."));
			}
			
		} else {
			echo json_encode(array("result"=>"0","mje"=>"No se obtubo alerta, favor de volver a intentar."));
		}
		
	}
}


?>