<?php
namespace MicrosoftAzure\Storage\Samples;
require_once "vendor/autoload.php";
require_once "blob_basic.php";
require_once "crear_json.php";
require_once "cnx.php";

date_default_timezone_set('UTC');
$timestamp=date('Y-m-d H:i:s', time());

echo "Hora UTC: $timestamp \n";
$dateFile = date('YmdHis', time());


// Ejecuta procedimiento almacenado
//$nomSP = "sp_estimado_real";
$nomSP = "sp_tiempo_estimado_real"; // SP de BDD Desarrollo SQM


$tsql_callSP = "{call ".$nomSP."}";  

$stmt3 = sqlsrv_query( $conn, $tsql_callSP);  
if( $stmt3 === false ) {  
	 echo "\nError al ejecutar sp_inserta_progreso.";  
	 die( print_r( sqlsrv_errors(), true));  
} else {
	echo "\nSe ejecuto correctamente.";
}


?>