<?php
require_once "controller/manifiestoController.php";
require_once "controller/class.inputfilter_clean.php";
$list = new manifiestoController();	

$sanitizer = new InputFilter();
foreach($_POST as $key=>$val){
	$arr[$key] =  $sanitizer->process($val);
}

switch(intval($arr['opAcc'])){
	case 1:		
		// Elimina Manifiesto
		echo $list->deletemanifiesto($arr['manif']);
		break;	
	case 2:
		// Graba Manifiesto.
		
		// valida que se obtenga una tonelada valida.
		for($i=1; $i<=10; $i++){
			if(@intval($arr['select_hold_'.$i]) != 0 && @intval($arr['select_mat_'.$i])!=0 && @intval($arr['tons_'.$i])==0){
				echo json_encode(array("result"=>"0","mje"=>"Debe ingresar las toneladas de cada material seleccionado (Load No. ".$i.")."));				
				exit();	
			}			
		}
		
		$list->guardarmanifiesto($arr);		
		break;
	case 3:	
		// Activa manifiesto
		$valor = $list->setActivaManifiesto($arr['manif']);
		if($valor){
			echo json_encode(array("result"=>"1","mje"=>"Se activo correctamente el manifiesto numero ".$arr['manif']."."));
		} else {
			echo json_encode(array("result"=>"0","mje"=>"Ubo un error al momento de activar el manifiesto, favor de intentarlo más tarde"));
		}
		break;
	case 4:
		// despliega listado de manifiestos
		$list->getData();
		break;
	case 5:
		// Actualiza Manifiestos
		
		// valida que se obtenga una tonelada valida.
		for($i=1; $i<=10; $i++){
			if(@intval($arr['select_hold_'.$i]) != 0 && @intval($arr['select_mat_'.$i])!=0 && @intval($arr['tons_'.$i])==0){
				echo json_encode(array("result"=>"0","mje"=>"Debe ingresar las toneladas de cada material seleccionado (Load No. ".$i.")."));				
				exit();	
			}			
		}
		
		$list->updatemanifiesto($arr);
		break;
	case 6:				
		// NO SE OCUPA
		
		// trae detalle de material
		//$list->getdetallematerialid($arr['mat']);			
		break;
	default:
		exit();
		break;
}


?>