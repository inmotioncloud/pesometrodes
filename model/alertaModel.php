<?PHP

require_once "config/class.conex.php";

class alertaModel{
	private $db;	
	public $txtMotivo;
	
	function alertaModel(){
		$this->db = new conexion();
	}
	
	// Obtiene listado de motivos de alerta
	public function getListMotivo(){
				
		$qry = "SELECT * FROM [dbo].[alerta_motivo] ORDER BY [cod_alerta] ASC;";	
		
		$resp = $this->db->db_query($qry);
		$rows = array();
		
		while($row = $this->db->db_fetch_object($resp)){	
			$row->motivo = utf8_encode($row->motivo);			
			array_push($rows ,$row);
		}
		
		return json_encode($rows);
	}
	
	public function getData(){
		
		$idManifiestoActivo = $this->getActivoManifiesto();
		$query = "SELECT * FROM [dbo].[alerta_pesometro] WHERE [avisado] = 1 AND [manifiesto] = ".intval($idManifiestoActivo)." ORDER BY [fch_hr_inicio] DESC;";
		$result = $this->db->db_query($query);
		$rows = array();
				
		while($row = $this->db->db_fetch_object($result)){			
						
			$row->acciones = (is_null($row->cod_alerta))? '<button type="button" class="btn btn-primary btn-sm" data-toggle="modal" data-target="#modalAgregarMotivo" onClick="openModal('.intval($row->id).')"><i class="far fa-plus-square"></i> Agregar motivo</button>':'';
			
			$row->cod_alerta = (is_null($row->cod_alerta))? "---":$row->cod_alerta;
			$row->motivo = (is_null($row->motivo))? "---":$row->motivo;
			$row->fch_hr_inicio = (is_null($row->fch_hr_inicio)? "---":date_format($row->fch_hr_inicio, 'Y-m-d H:i:s'));
			$row->fch_hr_termino = (is_null($row->fch_hr_termino)? "---":date_format($row->fch_hr_termino, 'Y-m-d H:i:s'));
			$row->fch_hr_motivo = (is_null($row->fch_hr_motivo)? "---":date_format($row->fch_hr_motivo, 'Y-m-d H:i:s'));			
			array_push($rows ,$row);
		}
		
		return json_encode(array("data"=>$rows));
	}

	function setActivarSiguienteBodega(){
		$idMan = $this->getActivoManifiesto();

		$query = "SELECT * FROM [manifiesto_det] WHERE [manifiesto] = ".intval($idMan)."  AND [material] !=0 ORDER BY [indice];";
		$result = $this->db->db_query($query);
		$rows = array();
	
		$bodegaActual = 0;
		$bodegaSiguiente = 0;

		while($row = $this->db->db_fetch_object($result)){
			
			if($bodegaActual!=0){
				//reemplace activo
				$bodegaSiguiente = intval($row->hold);
				$q2 = "UPDATE [manifiesto_det] SET [activo] = 1 WHERE [manifiesto] = ".intval($idMan)." AND  [indice] = ".$row->indice;
				$r2 = $this->db->db_query($q2);
				break;
			}

			if($row->activo == 1){
				// reemplaza la bodega activa con 0
				$bodegaActual = intval($row->hold);
				$q1 = "UPDATE [manifiesto_det] SET [activo] = 0 WHERE [manifiesto] = ".intval($idMan)." AND  [activo] = 1";
				$r1 = $this->db->db_query($q1);				
			}		
		}
		
		return 	json_encode(array("bodegaActual"=>$bodegaActual, "bodegaSiguiente"=>$bodegaSiguiente, "tt"=>$idMan));
	}

	private function getActivoManifiesto(){
		$query = "SELECT TOP(1) id FROM [dbo].[manifiesto_cab] WHERE [activo] = 1;";
		$result = $this->db->db_query($query);
		$row = $this->db->db_fetch_object($result);
		return intval($row->id);
	}
	
	public function existeMotivo($codMotivoAlerta){
		// buscar el motivo ingresado en bdd.
		$qry = "SELECT * FROM [dbo].[alerta_motivo] WHERE [cod_alerta] = ".intval($codMotivoAlerta);
		$result = $this->db->db_query($qry);
		$row = $this->db->db_fetch_object($result);
		
		// pregunta si existe el motivo.
		if (count($row)>0){			
			$this->txtMotivo = $row->motivo;
			return true;
		} else { 
			return false;
		}
	}
	
	public function existeAlerta($idAlerta){
		// Busca si existe la alerta.
		$qry = "SELECT [id] FROM [dbo].[alerta_pesometro] WHERE [ID] =  ".intval($idAlerta);
		$result = $this->db->db_query($qry);
		$row = $this->db->db_fetch_object($result);
		
		// pregunta si existe la alerta.
		if (count($row)>0){
			return true;
		} else { 
			return false;
		}
	}
		
	public function setMotivoAlerta($codAlerta, $motivo, $idAlerta){
		
		// Se actualiza el motivo de la alerta
		$qry = "UPDATE [dbo].[alerta_pesometro] SET [cod_alerta] = ".intval($codAlerta).", [motivo] = '".$motivo."', [fch_hr_motivo] = GETDATE() WHERE [id] = '".intval($idAlerta)."'";
		$result = $this->db->db_query($qry);
		
		if( $result === false ) {
			return false;
		} else {
			return true;
		}
		
	}
}


?>