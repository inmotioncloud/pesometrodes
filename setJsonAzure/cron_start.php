<?php
namespace MicrosoftAzure\Storage\Samples;
require_once "vendor/autoload.php";
require_once "blob_basic.php";
require_once "crear_json.php";
require_once "cnx.php";

date_default_timezone_set('UTC');
$timestamp=date('Y-m-d H:i:s', time());

echo "Hora UTC: $timestamp \n";
$dateFile = date('YmdHis', time());

// Creación de archivo JSON
$crear_json = new crear_json();
$crear_json->init($conn);
// se obtiene nombre de archivo a subir.
$fileName = $crear_json->getFileName();



// proceso de subida de archivo hacia Azure.
if (file_exists($fileName)) {
	// subir archivo hacia Azure
	$blobBasicSamples = new BlobBasicSamples();
	
	$blobBasicSamples->runUploadFile("pesometrocontainerdes", $fileName);
	//$blobBasicSamples->runUploadFile("pesometrocontainer", $fileName);
	
	// Eliminar archivo local
	//unlink($fileName);
	
} else {
	echo "No se encuentra archivo JSON para subir hacia Azure";
}


?>