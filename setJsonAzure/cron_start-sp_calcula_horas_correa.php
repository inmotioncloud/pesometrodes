<?php
namespace MicrosoftAzure\Storage\Samples;
require_once "vendor/autoload.php";
require_once "blob_basic.php";
require_once "crear_json.php";
require_once "cnx.php";

date_default_timezone_set('UTC');
$timestamp=date('Y-m-d H:i:s', time());

echo "Hora UTC: $timestamp \n";
$dateFile = date('YmdHis', time());


// Ejecuta procedimiento almacenado
//$nomSP = ""; // SP no existe en Desarrollo InMotion
$nomSP = "sp_calcula_horas_correa"; // SP de BDD Desarrollo SQM

$tsql_callSP = "{call ".$nomSP."}";  

$stmt3 = sqlsrv_query( $conn, $tsql_callSP);

if( $stmt3 === false ) {  
	 echo "\nError al ejecutar sp_calcula_horas_correa.";  
	 die( print_r( sqlsrv_errors(), true));  
} else {
	echo "\nSe ejecuto correctamente.";
}

?>