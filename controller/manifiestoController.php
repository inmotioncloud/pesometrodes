<?php
require_once "model/manifiestoModel.php";
require_once "model/materialesModel.php";
require_once "controller/class.inputfilter_clean.php";

class manifiestoController {

    private $manifiesto;
    private $materiales;
	
	function manifiestoController(){
		$this->manifiesto = new manifiestoModel();	
		$this->materiales = new materialesModel();		
    } 
    
    public function getData(){		
		echo $this->manifiesto->getData();		
	}

	public function getcabmanif($id){
		return $this->manifiesto->getCabManifiesto($id);
	}

	public function getdetmanif($id){
		$val =  $this->manifiesto->getDetManifiesto($id);		
		return $val;
	}

	public function getbodmanif($id){
		return $this->manifiesto->getBodegaMaterialManifiesto($id);
	}

	public function getDataMateriales(){

		$listMateriales = $this->materiales->getMateriales();
		return $listMateriales;
	
	}

	public function getDetalleMaterial($valor)
	{
		
		$detalleMaterial = $this->materiales->getDetalleMaterial($valor); 		
		
		$json = json_encode($detalleMaterial);        
        echo $json;
	}

	public function getdetallematerialid()
	{
		if(isset($_POST['mat']) && $_POST['mat'] > 0 && $_POST['mat'] != '')
		{
			$material=$_POST['mat'];
			$materiales=$this->materiales->getDetalleMaterialId($material);
			$json = json_encode($materiales);        
			echo $json;		
		}
	}
	
	public function setActivaManifiesto($idMantenedor){
		return $this->manifiesto->setActivaManifiesto($idMantenedor);		
	}

	public function guardarmanifiesto($arr){
		
		// Ordena fecha de inicio para grabación en BDD.
		$fechaInicio=$arr['fecha_inicio'];
		$fechaInicio=explode("-",$fechaInicio);
		$fechaInicio = $fechaInicio[0]."-".$fechaInicio[1]."-".$fechaInicio[2];
		
		// Graba cabecera del manifiestop
		$manifiestoId = $this->manifiesto->guardarManifiesto($arr['puerto_origen'], $arr['puerto_destino'], $fechaInicio);
		
		if(intval($manifiestoId)!=0){
			// Se guardo cabecera exitosamente			
			
			// for de listado de materiales de holds
			for($j=1; $j<=5; $j++){				
				$hold = $j ;
				if(isset($arr['select_hold_'.$j.'_1'])){
					$mat_1 = $arr['select_hold_'.$j.'_1'];
				}else{
					$mat_1 = "";
				}
				
				if(isset($arr['select_hold_'.$j.'_2'])){
					$mat_2 = $arr['select_hold_'.$j.'_2'];
				}else{
					$mat_2 = "";
				}
				
				if(isset($arr['select_hold_'.$j.'_3'])){
					$mat_3 = $arr['select_hold_'.$j.'_3'];
				}else{
					$mat_3 = "";
				}
					
				// graba material 
				if(!$this->manifiesto->guardarManifiestoMaterial( $manifiestoId , $hold , $mat_1 , $mat_2 , $mat_3 )){					
					// no se grabo un material
					echo json_encode(array("result"=>"0","mje"=>"No se pudo grabar el material, favor de revisar y volver a intentar."));
					exit();
				}				
			}
			
			// for de indices de carga en relacion con los materiales de holds
			for($k=1; $k<=10; $k++){
				
				if(isset($arr['select_hold_'.$k])){
					$hold = $arr['select_hold_'.$k];
				}else{
					$hold = 0;
				}

				if(isset($arr['select_mat_'.$k])){
					$material = $arr['select_mat_'.$k];
				}else{
					$material = 0;
				}

				if(isset($arr['tons_'.$k])){
					$tons = $arr['tons_'.$k];
				}else{
					$tons = 0;
				}
				
				
				if($hold!=0 && $material==0){
					$hold = 0;
					$tons = 0;
				}
			
				
				// Graba indice del manifiesto						
				if(!$this->manifiesto->guardarManifiestoDetalle( $manifiestoId , $k , $hold ,  $tons  , $material )){
					echo json_encode(array("result"=>"0","mje"=>"No se pudo grabar el material, favor de revisar y volver a intentar."));			
					exit();
				}				
			}
			
			// si todo se grabo exitosamente envia mensaje:
			echo json_encode(array("result"=>"1","mje"=>"El Manifiesto fue guardado exitosamente.","url"=>"mtManifiesto.php"));
			
		} else {
			// No se pudo grabar cabezera
			echo json_encode(array("result"=>"0","mje"=>"Ubo un error al grabar los datos, favor de volver a intentar."));
		}		

	} // Fin metodo
	//-------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
	public function deletemanifiesto( $manif ){
		
		if($this->manifiesto->deleteManifiesto(intval($manif))){
			echo json_encode(array("result"=>"1","mje"=>"El manifiesto fue eliminado exitosamente."));
		} else {
			echo json_encode(array("result"=>"0","mje"=>"Ubo un error al eliminar el manifiesto, favor de volver a intentar."));
		}
	}
	
	public function updatemanifiesto($arr){
		
		// Id de Manifiesto
		$manifiesto = $_POST['manifiestoId'];
		
		// Ordena fecha de inicio para grabación en BDD.
		$fechaInicio=$arr['fecha_inicio'];
		$fechaInicio=explode("-",$fechaInicio);
		$fechaInicio = $fechaInicio[0]."-".$fechaInicio[1]."-".$fechaInicio[2];

		if(!$this->manifiesto->updateManifiesto($arr['manifiestoId'], $arr['puerto_origen'] , $arr['puerto_destino'], $fechaInicio)){
			// No se pudo actualizar cabezera
			echo json_encode(array("result"=>"0","mje"=>"Ubo un error al actualizar los datos, favor de volver a intentar."));
			exit();
		}
		
		
		for($j=1;$j<=5;$j++)
		{
			$hold = $j ;
			if(isset($arr['select_hold_'.$j.'_1'])){
				$mat_1 = $arr['select_hold_'.$j.'_1'];
			}else{
				$mat_1 = "";
			}
			if(isset($arr['select_hold_'.$j.'_2'])){
				$mat_2 = $arr['select_hold_'.$j.'_2'];
			}else{
				$mat_2 = "";
			}
			if(isset($arr['select_hold_'.$j.'_3'])){
				$mat_3 = $arr['select_hold_'.$j.'_3'];
			}else{
				$mat_3 = "";
			}
		
			// actualiza lista de materiales
			if(!$this->manifiesto->updateManifiestoMaterial( $arr['manifiestoId'] , $hold , $mat_1 , $mat_2 , $mat_3)){
				echo json_encode(array("result"=>"0","mje"=>"No se pudo grabar el material, favor de revisar y volver a intentar."));		
				exit();
			}
		}

		for($k=1;$k<=10;$k++){
			$indice =$k;

			if(isset($arr['select_hold_'.$k])) {
				$hold = $arr['select_hold_'.$k];
			}else{
				$hold = "0";
			}

			if(isset($arr['select_mat_'.$k])){
				$material = $arr['select_mat_'.$k];
			}else{
				$material = "0";
			}

			if(isset($arr['tons_'.$k])){
				$tons = $arr['tons_'.$k];
			}else{
				$tons = "0";
			}
			
			if($hold!=0 && $material==0){
				$hold = "0";
				$tons = "0";
			}
			
			// Actualiza indice del manifiesto						
			if(!$this->manifiesto->updateManifiestoDetalle( $arr['manifiestoId'] , $indice , $hold , $tons , $material )){
				echo json_encode(array("result"=>"0","mje"=>"No se pudo actualizar el material, favor de revisar y volver a intentar.<br> COD:".$arr['manifiestoId']."-$indice-$hold-$tons-$material"));				
				exit();
			}	
			
		}
		
		// si todo se actualiza exitosamente envia mensaje:
		echo json_encode(array("result"=>"1","mje"=>"El Manifiesto fue actualizado exitosamente.","url"=>"mtManifiesto.php"));

	}
	
	function esActivoManifiesto($idManiniesto){
		$valor = $this->manifiesto->esActivoManifiesto(intval($idManiniesto));
		if( $valor >=1){
			return 1;
		} else {
			return 0;
		}		
	}
}
    
    
    
?>
