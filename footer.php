    <!-- Footer -->
    <footer class="py-3 bg-success">
      <div class="container">
        <p class="m-0 text-center text-white">Copyright &copy; SQM <?=date("Y")?></p>
      </div>
    </footer>	  