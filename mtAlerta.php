<?php
require_once "controller/alertaController.php";

?>

<!DOCTYPE html>
<html lang="en">

  <head>
	<?php require_once "meta.php";?>
	<style>
		.nav-pills .nav-link.active, .nav-pills .show > .nav-link {
			color: #fff !important;
			background-color: #ccc;
		}		
		#txtIngresoOtro{
			display: none;
		}
		#mjeAdvertencia{
			display: none;
		}
	</style>
  </head>

  <body>

	<?php require_once "menu.php";?>

    <!-- Page Content -->
    <div class="container">

      <!-- Page Heading/Breadcrumbs -->
      <h1 class="mt-4 mb-3"><i class="fas fa-exclamation-triangle"></i> Alertas</h1>

      <ol class="breadcrumb">
        <li class="breadcrumb-item">
          <a href="index.html">Home</a>
        </li>
        <li class="breadcrumb-item active">Alertas</li>
      </ol>

      <div class="row">

        <!-- Blog Entries Column -->
        <div class="col-md-12">

          <!-- Blog Post -->
          <div class="card mb-4">        
            <div class="card-body">
				<button type="button" class="btn btn-info btn-sm" id="btnRefrescar"><i class="fas fa-redo-alt"></i> Refrescar Alertas</button><br><br>
				
				
				<table id="tblAlertas" class="table table-striped table-bordered display nowrap" style="width:100%">
					 <thead>
						<tr>
							<th>Manifiesto</th>
							<th>Motivo</th>
							<th>Inicio</th>
							<th>Término</th>
							<th>Ingreso Motivo</th>
							<th class="text-center">Acciones</th>
						</tr>
					</thead>					
				</table>
				

            </div>
            <div class="card-footer text-muted"></div>
          </div>

        </div>

        </div>

      </div>
      <!-- /.row -->

    </div>
    <!-- /.container -->

	<?php require_once "footer.php";?>
	


	<!-- Modal -->
	<div class="modal fade" id="modalAgregarMotivo" name="modalAgregarMotivo" tabindex="-1" role="dialog" aria-labelledby="exampleModalLongTitle" aria-hidden="true">
	  <div class="modal-dialog modal-dialog-centered" role="document">
		<div class="modal-content">
		  <div class="modal-header">
			<h5 class="modal-title" id="exampleModalLongTitle">Ingreso de Motivo</h5>
			<button type="button" class="close" data-dismiss="modal" aria-label="Close">
			  <span aria-hidden="true">&times;</span>
			</button>
		  </div>
		  <div class="modal-body">
		  
		  	<form id="frmIngresoMotivo">
				<input id="idAlerta" name="idAlerta" type="hidden" value="0">
				<div class="form-group">			
					<select id="opMotivo" name="opMotivo" class="form-control" id="opMotivo">
					
					<?php
						$list = new alertaController();		
						$list->getListaMotivo();
					?>
					
					</select>
				</div>

				<div id="txtIngresoOtro">
					<div class="form-group">
						<label for="txtMotivo">Ingrese motivo:</label>
						<input class="form-control" id="txtMotivo" name="txtMotivo" maxlength="50" />
						<span style="font-size:10px; color:#999;">Máximo 50 caracteres</span>
					</div>
					<div id="mjeAdvertencia" class="alert alert-warning"><strong>Advertencia!</strong> <p>Debe ingresar motivo de la alerta.</p></div>
				</div>
			</form>
			
		  </div>
		  <div class="modal-footer">
			<button type="button" class="btn btn-secondary" data-dismiss="modal"><i class="fas fa-ban"></i> Cancelar</button>
			<button type="button" class="btn btn-primary" id="btnGuardarMotivo" disabled><i class="far fa-save"></i> Guardar motivo</button>
		  </div>
		</div>
	  </div>
	</div>

	<?php require_once "js.php";?>

	<script>
	var idOtro = 9; // Id de campo de Otro
	$(document).ready(function(){
	
		$('#tblAlertas').DataTable({			
			"ajax": {
				"url": 'alerta_ajax.php',
				"type": 'POST',
				"data": {"opAcc": 1}
			},
			"language": {
				"sProcessing":     "Procesando...",
				"sLengthMenu":     "Mostrar _MENU_ registros",
				"sZeroRecords":    "No se encontraron resultados",
				"sEmptyTable":     "Ningún dato disponible en esta tabla",
				"sInfo":           "Mostrando _START_ de _END_ de _TOTAL_ registros",
				"sInfoEmpty":      "Mostrando 0 de 0 de 0 registros",
				"sInfoFiltered":   "(filtrado de un total de _MAX_ registros)",
				"sInfoPostFix":    "",
				"sSearch":         "Buscar:",
				"sUrl":            "",
				"sInfoThousands":  ",",
				"sLoadingRecords": "Cargando...",
				"oPaginate": {
					"sFirst":    "Primero",
					"sLast":     "Último",
					"sNext":     "Siguiente",
					"sPrevious": "Anterior"
				},
				"oAria": {
					"sSortAscending":  ": Activar para ordenar la columna de manera ascendente",
					"sSortDescending": ": Activar para ordenar la columna de manera descendente"
				}
			},
			dataSrc: 'data',
			columns: [
				{ data: 'manifiesto' , "sClass": "center"  },				
				{ data: 'motivo' },
				{ data: 'fch_hr_inicio' },
				{ data: 'fch_hr_termino' },
				{ data: 'fch_hr_motivo' },
				{ data: 'acciones' }
			],
			order: [[ 2, "desc" ]],
			columnDefs: [			
				{ "className": "dt-center", "targets": "_all" }
			],			
			responsive: true
		});
		
		$("#opMotivo").on("change",function(){
			var id = $(this).children(":selected").attr("id");
						
			if(id==idOtro){ // id de Otro
				$("#txtIngresoOtro").show();
			} else {
				$("#txtMotivo").val('');
				$("#txtIngresoOtro").hide();
			}
			
			if(id==0){
				$("#btnGuardarMotivo").prop('disabled', true);				
			} else {
				$("#btnGuardarMotivo").prop('disabled', false);			
			}
			
		});
		
		$("#btnGuardarMotivo").on("click",function(){
			
			$("#mjeAdvertencia").hide();
			if($("#opMotivo").children(":selected").attr("id") != 0){
				
				if($("#opMotivo").children(":selected").attr("id") == idOtro){
					if($("#txtMotivo").val() ==''){
						$("#mjeAdvertencia").show();
					} else {
						// grabar datos
						grabarMotivo();
					}
				}else {
					// grabar datos
					grabarMotivo();
				}				
			}
			
		});
		$("#btnRefrescar").on("click",function(){ 
			HoldOn.open({theme:'sk-rect', message:"<h4>Cargando...</h4>"});			
			$('#tblAlertas').DataTable().ajax.reload();	
			HoldOn.close();
		});
		
		jQuery.extend( jQuery.fn.dataTableExt.oSort, {
			"date-uk-pre": function ( a ) {
				var ukDatea = a.split('/');
				return (ukDatea[2] + ukDatea[1] + ukDatea[0]) * 1;
			},

			"date-uk-asc": function ( a, b ) {
				return ((a < b) ? -1 : ((a > b) ? 1 : 0));
			},

			"date-uk-desc": function ( a, b ) {
				return ((a < b) ? 1 : ((a > b) ? -1 : 0));
			}
		});

	});
	
	function grabarMotivo(){
		
		var arr = $("#frmIngresoMotivo").serialize();	

		$.ajax({
			url: "alerta_ajax.php",
			type: "POST",			
			data: "&opAcc=2&"+arr,
			beforeSend : function (){
				HoldOn.open({theme:'sk-rect', message:"<h4>Cargando...</h4>"});
			},
			success: function(response){

				var data = JSON.parse(response);
				
				if(data.result==1){	
					bootbox.alert(data.mje);
					HoldOn.close();
					$('#modalAgregarMotivo').modal('hide');
					$('#tblAlertas').DataTable().ajax.reload();								
				} else {
					bootbox.alert(data.mje);
					HoldOn.close();
				} 
				
				
			}
		});//end ajax
		
	}
	
	function openModal(idAlerta){
		// resetear ventana motivo.
		$('#opMotivo').prop('selectedIndex',0);
		$("#txtMotivo").val('');
		$("#txtIngresoOtro").hide();
		$("#btnGuardarMotivo").prop('disabled', true);
			
		// levantar ventana motivo.
		$("#idAlerta").val(idAlerta);
		$('#modalAgregarMotivo').modal('show'); 
	}
	</script>
  </body>

</html>
