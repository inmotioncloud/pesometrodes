<?php
require_once "controller/correaController.php";

?>

<!DOCTYPE html>
<html lang="en">

  <head>
	<?php require_once "meta.php";?>
	<style>
		.nav-pills .nav-link.active, .nav-pills .show > .nav-link {
			color: #fff !important;
			background-color: #ccc;
		}
		
	</style>
  </head>

  <body>

	<?php require_once "menu.php";?>

    <!-- Page Content -->
    <div class="container">

      <!-- Page Heading/Breadcrumbs -->
      <h1 class="mt-4 mb-3"><i class="fas fa-cogs"></i> Correa</h1>

      <ol class="breadcrumb">
        <li class="breadcrumb-item">
          <a href="index.html">Home</a>
        </li>
        <li class="breadcrumb-item active">Correa</li>
      </ol>

      <div class="row">

        <!-- Blog Entries Column -->
        <div class="col-md-12">

          <!-- Blog Post -->
          <div class="card mb-4">        
            <div class="card-body">
				<button type="button" class="btn btn-info btn-sm" id="btnRefrescar"><i class="fas fa-redo-alt"></i> Refrescar datos</button><br><br>
				
				<div class="row">
					<div class="col">
						<h1><strong><span id="txtNombre">...</span></strong></h1>
						<br>
						<h3>Uso de correa:</h3>
						<h2><span id="txtUsoCorreas">...</span> Horas.</h2>
						<br>
						<button type="button" id="btnResetCorrea" class="btn btn-danger btn-xl xs-block"><i class="fas fa-retweet"></i> Resetear contador uso de correa</button>
					</div>
					<div class="col">
						<table class="table table-striped">
						  <thead>
							<tr class="text-center">
							  <th scope="col">Num. de alerta</th>
							  <th scope="col">Horas de uso</th>
							  <th scope="col">Fecha alerta</th>
							</tr>
						  </thead>
						  <tbody>
							<tr class="text-center">
							  <th scope="row">1</th>
							  <th scope="row">3000</th>
							  <td><span id="alerta_1">---</span></td>
							</tr>
							<tr class="text-center">
							  <th scope="row">2</th>
							  <th scope="row">3500</th>
							  <td><span id="alerta_2">---</span></td>			  
							</tr>
							<tr class="text-center">
							  <th scope="row">3</th>
							  <th scope="row">4000</th>
							  <td><span id="alerta_3">---</span></td>
							</tr>
							<tr class="text-center">
							  <th scope="row">4</th>
							  <th scope="row">4500</th>
							  <td><span id="alerta_4">---</span></td>
							</tr>
							
						  </tbody>
						</table>
					</div>
				</div>
			
				
            </div>
            <div class="card-footer text-muted"></div>
          </div>

        </div>

        </div>

      </div>
      <!-- /.row -->

    </div>
    <!-- /.container -->

	<?php require_once "footer.php";?>

	<?php require_once "js.php";?>

	<script>
	$(document).ready(function(){
		
		rellenaDatos();		
		
		$("#btnResetCorrea").on("click", function(){
			bootbox.confirm({
				closeButton: false,
				title: "Advertencia",
				message: "¿Está realmente seguro de querer reiniciar el contador de Horas de uso de correa?",
				buttons: {
					cancel: {
						label: 'Cancelar',
						className: 'btn-secondary'
					},
					confirm: {
						label: 'Reiniciar',
						className: 'btn-danger'
					}
					
				},				
				callback: function (result) {
					if(result){
						// reiniciar contador
						
						$.ajax({
						url: "correa_ajax.php",
						type: "POST",			
						data: {"opAcc": 2},
						beforeSend : function (){
							HoldOn.open({theme:'sk-rect', message:"<h4>Cargando...</h4>"});
						},
						success: function(response){						
							var data = JSON.parse(response);				
							
							if(data.result==1){	
								bootbox.alert(data.mje);								
								rellenaDatos();
							} else {
								bootbox.alert(data.mje);
								HoldOn.close();
							} 							
							
						}
					});//end ajax
					}
					
				}
			});
			
		});
		
		$("#btnRefrescar").on("click",function(){ 
			rellenaDatos();
		});

	});
	
	function rellenaDatos(){
		$.ajax({
			url: "correa_ajax.php",
			type: "POST",			
			data: {"opAcc": 1},
			beforeSend : function (){
				HoldOn.open({theme:'sk-rect', message:"<h4>Cargando...</h4>"});
			},
			success: function(response){						
				var data = JSON.parse(response);				
				$("#txtNombre").html(data[0].nombre_correa);
				$("#txtUsoCorreas").html(data[0].hrs_uso_correa);
				$("#alerta_1").html(data[0].fch_alerta_uno);
				$("#alerta_2").html(data[0].fch_alerta_dos);
				$("#alerta_3").html(data[0].fch_alerta_tres);
				$("#alerta_4").html(data[0].fch_alerta_cuatro);
				HoldOn.close();
			}
		});//end ajax
	}

	</script>
  </body>

</html>
