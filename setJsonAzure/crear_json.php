<?php
namespace MicrosoftAzure\Storage\Samples;
require_once "vendor/autoload.php";

class crear_json{
	
	private $arr;
	private $cc;
	private $tons_acu;
	private $tons;
	private $nameFile;
	private $arrManDet;
	private $acumTermHoldTm;
	private $minutosIntervaloAlerta;
	
	public function init($conn){	
		
		$this->cc = $conn;
		$this->arr = array();
		$this->arrManDet = array();
		$this->tons_acu = 0;
		$this->tons = 0;
		$this->nameFile = '';
		$this->acumTermHoldTm = 0;
		$this->minutosIntervaloAlerta = 3;
		
		$this->getManifiesto();
		$this->getMotonave();
		$this->getDestino();
		$this->getMaterial();
		$this->getFlujoMinMax();
		$this->getHold_tm();
		$this->getHold_tm_progress();
		$this->getHold_state();
		$this->getCantidadAlertas();
		$this->getExisteAlerta();
		$this->getHorasCorrea();		
		
		$this->crearJson();
		
	}	
	
	private function getManifiesto(){
		//Obtiene numero de manifisto.
		
		//OLD: $query = "SELECT max(manifiesto) as 'manifiesto' FROM [manifiesto_det];";
		$query = "SELECT TOP (1) id AS 'manifiesto' FROM [manifiesto_cab] WHERE [activo] = 1;";
		$result = sqlsrv_query($this->cc,$query)or die(json_encode(sqlsrv_errors()));
		$row = sqlsrv_fetch_array($result);		
		$this->arr['manifiesto'] = intval($row['manifiesto']);
	}
	
	private function getMotonave(){
		// Aún no hay datos 
		$this->arr['motonave'] = 1;
	}
	private function getDestino(){	
		//	obtiene destino según el manifiesto
		$query = "SELECT puerto_destino FROM [manifiesto_cab] WHERE id = ".$this->arr['manifiesto'].";";
		$result = sqlsrv_query($this->cc,$query)or die(json_encode(sqlsrv_errors()));
		$row = sqlsrv_fetch_array($result);		
		$this->arr['destino'] = $row['puerto_destino'];
	}
	
	private function getMaterial(){
		// Obtiene material y hold
		$query = "SELECT TOP (1) * FROM [dbo].[lecturas_pesometro] ORDER BY [fechahora] DESC;";
		$result = sqlsrv_query($this->cc,$query)or die(json_encode(sqlsrv_errors()));
		$row = sqlsrv_fetch_object($result);	
		
		$this->tons = intval($row->tons);
		
		// obtiene toneladas por hora.
		$this->arr['tons_hr'] = intval($row->tons_hr); 
		
		// procesa información para crear una alerta o no.
		$this->setAlertaPesometro($row->fechahora, $row->tons_hr);
			
		$query = "SELECT * FROM [manifiesto_det] WHERE [manifiesto] = ".$this->arr['manifiesto']."  AND [material] !=0 ORDER BY [indice];";
		$result = sqlsrv_query($this->cc,$query)or die(json_encode(sqlsrv_errors()));

		while($rows = sqlsrv_fetch_object($result)){
			// Crea array de manifiesto_det			
			array_push($this->arrManDet, $rows);			
			
			$this->tons_acu += $rows->tons;
			//if($this->tons <= $this->tons_acu && $this->activol){
			if($rows->activo == 1){
				$this->arr['material'] = intval($rows->material);
				$this->arr['hold'] = intval($rows->hold);				
				break;
			} else {
				$this->acumTermHoldTm += $rows->tons;
			}
		}		
		
		if($this->tons_acu < $this->tons){
			$this->arr['material'] = 'exedido';
		}
	}
	private function getFlujoMinMax(){
		// se obtiene flujo minimo y maximo, obteniendo el id de material.
		
		if(intval($this->arr['material'])!=0){
			$query = "SELECT * FROM [dbo].[materiales] WHERE [id] = ".intval($this->arr['material']).";";
			$result = sqlsrv_query($this->cc,$query)or die(json_encode(sqlsrv_errors()));
			$row = sqlsrv_fetch_array($result);		
			$this->arr['flujo_min'] = intval($row['min_vel']);
			$this->arr['flujo_max'] = intval($row['max_vel']);			
		} else {
			$this->arr['flujo_min'] = 'no existe material';
			$this->arr['flujo_max'] = 'no existe material';
		}
		
	}
	private function getHold_tm(){
		// obtenemos el total de toneladas por bodega(hold)
		$query = "SELECT SUM(tons)as 'hold_tm' FROM [manifiesto_det] WHERE [manifiesto] = ".$this->arr['manifiesto']." AND [hold] = ".$this->arr['hold'].";";
		$result = sqlsrv_query($this->cc,$query)or die(json_encode(sqlsrv_errors()));
		$row = sqlsrv_fetch_array($result);
		$this->arr['hold_tm'] = intval($row['hold_tm']);
	}
	private function getHold_tm_progress(){
		
		$rest = $this->tons - $this->acumTermHoldTm;		
		$this->arr['hold_tm_progress'] = ($rest * 100) / ($this->arr['hold_tm'] + ($this->arr['hold_tm'] * 0.01));
		$this->arr['hold_progress'] = $rest;
		
	}
	
	private function getHold_state(){
		// obtiene el estado de remate
		$this->arr['estado_remate'] = ($this->arr['hold_tm'] + ($this->arr['hold_tm'] * 0.01));
	}
	private function getExisteAlerta(){
		// envia 1 si se encuentra una alertra activa o '0' cero si no hay una alerta activa
		$qry ="select count(*) as 'cant_alertas_activas'  from [dbo].[alerta_pesometro] where activo = 1;";
		$result = sqlsrv_query($this->cc, $qry)or die(json_encode(sqlsrv_errors()));
		$row = sqlsrv_fetch_object($result);
		$this->arr['estado_alerta'] = ($row->cant_alertas_activas>0)? 1:0;		
	}	
	
	private function setAlertaPesometro($fechahora, $tons_hr){
		echo "<hr>ALERTAS:<br>";
		// consulta si existe una alerta activa
		$qry_activa = "SELECT * FROM [dbo].[alerta_pesometro] WHERE [manifiesto] = ".$this->arr['manifiesto']." AND [activo] = 1;";
		$result = sqlsrv_query($this->cc, $qry_activa)or die(json_encode(sqlsrv_errors()));
		$row = sqlsrv_fetch_object($result);
		
		// si no existe una alerta activa entra al if
		if(count($row)==0){
			
			// consulta si existe una alerta en espera del tiempo requerido
			$qry_tiempo = "SELECT * FROM [dbo].[alerta_pesometro] WHERE [manifiesto] = ".$this->arr['manifiesto']." AND [fch_hr_termino] IS NULL AND [activo] = 0;";
			$result = sqlsrv_query($this->cc, $qry_tiempo)or die(json_encode(sqlsrv_errors()));
			$activa = false;
			$crearNuevaAlerta = true;		
		
			while($row = sqlsrv_fetch_object($result)){
				// recorre los registros y comprueba el tiempo transcurrido desde que se creo el registro hasta ahora, en minutos
				$fecha1 = date('Y-m-d H:i:s');
				$fecha2 = date_format($row->fch_hr_inicio, 'Y-m-d H:i:s');
				
				
				echo "Fecha Inicio: $fecha2 <br>";
				echo "Fecha Actual: $fecha1 <br>";
				
				$minutos = ceil((strtotime($fecha1) - strtotime($fecha2)) / 60);
				
				echo "- La alerta lleva $minutos minutos de espera...<br>";
				
				// si ya pasaron los minutos configurados, activa la alerta.
				if ($minutos >= $this->minutosIntervaloAlerta) {
					echo '- Se activa Alerta, lleva más de '.$this->minutosIntervaloAlerta.' minutos de espera.';
					// activar alerta					
					$qry_update = "UPDATE [dbo].[alerta_pesometro] SET [activo] = 1, [avisado] = 1 WHERE [id] = ".$row->id.";";
					sqlsrv_query($this->cc, $qry_update)or die(json_encode(sqlsrv_errors()));
					
				} else {
					if(intval($tons_hr)>0){
						echo "- En espera de alerta esta bajo los ".$this->minutosIntervaloAlerta." min, se desactiva por que llego tonelaje.";
						// da termino a la alerta, ya que llego pesaje en tons_hr
						$qry_update = "UPDATE [dbo].[alerta_pesometro] SET [fch_hr_termino] = '".date_format($fechahora, 'Y-m-d H:i:s')."', [activo] = 0 WHERE [id] = ".$row->id.";";
						sqlsrv_query($this->cc, $qry_update)or die(json_encode(sqlsrv_errors()));	
					}
				}
				
				$crearNuevaAlerta = false;
			}
			
			// crea una alerta de espera
			if($crearNuevaAlerta && intval($tons_hr) == 0){
				echo "- Se crea alerta de en espera, tons_hr llego en cero y no hay otras alertas.";
				$qry_crear = "INSERT INTO [dbo].[alerta_pesometro] ([manifiesto], [fch_hr_inicio]) VALUES (".$this->arr['manifiesto'].", '".date_format($fechahora, 'Y-m-d H:i:s')."');";
				sqlsrv_query($this->cc, $qry_crear)or die(json_encode(sqlsrv_errors()));	
			}			
			
		} else {
			echo "- Existe una alerta Activa. <br>";
			if(intval($tons_hr)>0){
				echo "- Existe 1 alerta, se desactiva por ke llego tonelaje.";
				// da termino a la alerta, ya que llego pesaje en tons_hr
				$qry_update = "UPDATE [dbo].[alerta_pesometro] SET [fch_hr_termino] = '".date_format($fechahora, 'Y-m-d H:i:s')."', [activo] = 0, [avisado] = 1 WHERE [id] = ".$row->id.";";
				sqlsrv_query($this->cc, $qry_update)or die(json_encode(sqlsrv_errors()));	
			}
		}
		
	}
	
	private function getCantidadAlertas(){
		// obtiene el total de alertas por manifiesto.
		$qry = "SELECT COUNT(*) AS 'cantAlertas' FROM [dbo].[alerta_pesometro] WHERE [manifiesto] = ".intval($this->arr['manifiesto'])." AND ([avisado] = 1 OR [activo] = 1);";
		$result = sqlsrv_query($this->cc, $qry)or die(json_encode(sqlsrv_errors()));
		$row = sqlsrv_fetch_object($result);
		$this->arr['cant_alertas'] = $row->cantAlertas;
	}
	
	public function setActualizaProgreso(){
		// ejecuta procedimeinto almacenado en SQLSERVER.		
		
		//$nomSP = "sp_inserta_progreso";
		$nomSP = "sp_inserta_actualiza_progreso";	// SP de BDD Desarrollo SQM
		
		$tsql_callSP = "{call ".$nomSP."( ?, ?, ?, ?)}";  
				
		$params = array(   
						array($this->arr['manifiesto'], SQLSRV_PARAM_IN),
						array($this->arr['hold'], SQLSRV_PARAM_IN),
						array($this->arr['material'] , SQLSRV_PARAM_IN),
						array($this->arr['estado_remate'], SQLSRV_PARAM_IN)  
						);  

	
		$stmt3 = sqlsrv_query( $this->cc, $tsql_callSP, $params);  
		if( $stmt3 === false ) {  
			 echo "Error al ejecutar sp_inserta_progreso.";  
			 die( print_r( sqlsrv_errors(), true));  
		} 

		  
	}
	
	public function getHorasCorrea() {
		// obtiene las horas de uso de correa
		$query = "SELECT hrs_uso_correa FROM [dbo].[alerta_correa]";		
		$result = sqlsrv_query($this->cc, $query)or die(json_encode(sqlsrv_errors()));
		$row = sqlsrv_fetch_object($result);
		$this->arr['hrs_correa'] = intval($row->hrs_uso_correa);		
	}
	
	private function crearJson(){	
		// se realiza creación de archivo fisico JSON.
		date_default_timezone_set('UTC');		
				
		$jsonencoded = json_encode($this->arr,JSON_UNESCAPED_UNICODE);		
		$this->nameFile = "eventos-".date("YmdHis", time()).".json";
		
		$fh = fopen($this->nameFile, 'w');
		fwrite($fh, $jsonencoded);
		fclose($fh);
		
		// Ejecuta procedimiento almacenado en SQL Server		
		$this->setActualizaProgreso();
		
		
		/*-------------------------------------------------------------------------------*/
		echo "<hr>\n\n";
		echo "Manifiesto Detalle - Array\n";
		echo "<pre>";
		print_r($this->arrManDet);
		echo "</pre>";
		
		
		echo "Manifiesto Detalle - Lectura\n";
		foreach($this->arrManDet as $key => $val){			
			foreach($val as $key => $val){
				echo "$key => $val <br>";
			}
		}
		
		echo "<hr>\n\n";
		
		echo "Se crea archivo JSON con la siguiente información:\n";
		echo "<pre>";
		print_r(json_decode($jsonencoded));
		echo "</pre>";
		echo "<hr>\n\n";
		/*-------------------------------------------------------------------------------*/
	}
	
	public function getFileName(){
		return $this->nameFile;
	}
}
