<!doctype html>
<html lang="en">
	<head>
		<?php require_once "meta.php";?>
		<!-- Custom styles for this template -->
		<link href="css/signin.css" rel="stylesheet">
	</head>
	<body class="text-center">
  
		<form class="form-signin">
			<img class="mb-4" src="img/SQM.png" alt="" width="150" height="150">      
			<label for="inputEmail" class="sr-only">Usuario</label>
			<input type="email" id="inputEmail" class="form-control" placeholder="Usuario" required autofocus>
			<label for="inputPassword" class="sr-only">Contraseña</label>
			<input type="password" id="inputPassword" class="form-control" placeholder="Contraseña" required>
				<div class="checkbox mb-3">			
				</div>
			<button id="btnIniciar" name="btnIniciar" class="btn btn-lg btn-success btn-block" type="button">Iniciar sessión</button>
			<p class="mt-5 mb-3 text-muted">&copy; <?=date("Y")?> Copyright SQM</p>
		</form>
		
	<?php require_once "js.php";?>
	<script>
	$(document).ready(function(){		
		$("#btnIniciar").on("click", function(e){			
			$(location).attr('href', 'mtManifiesto.php');
		});
	});
	</script>
	</body>	
</html>