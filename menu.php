<?php
$opUrl = explode("/",$_SERVER["REQUEST_URI"]);
?>
	<!-- Navigation -->
    <nav class="navbar fixed-top navbar-expand-lg navbar-light bg-light fixed-top">
      <div class="container">
        <img src="img/SQM.png" alt="" width="40" height="40"> Pesometro
        <button class="navbar-toggler navbar-toggler-right" type="button" data-toggle="collapse" data-target="#navbarResponsive" aria-controls="navbarResponsive" aria-expanded="false" aria-label="Toggle navigation">
          <span class="navbar-toggler-icon"></span>
        </button>
        <div class="collapse navbar-collapse" id="navbarResponsive">
		
          <ul class="navbar-nav ml-auto nav-pills nav-fill">
					  
            <li class="nav-item">			
				<a class="nav-link <?=(current(explode('.php',end($opUrl))) == 'mtManifiesto' || current(explode('.php',end($opUrl))) == 'mtManifiesto_add')? 'active':'';?>" href="mtManifiesto.php"><i class="far fa-list-alt"></i> Manifiesto</a>
            </li>
            <li class="nav-item">
				<a class="nav-link <?=(current(explode('.php',end($opUrl))) == 'mtAlerta')? 'active':'';?>" href="mtAlerta.php"><i class="fas fa-exclamation-triangle"></i> Alertas</a>
            </li>
			<li class="nav-item">
				<a class="nav-link <?=(current(explode('.php',end($opUrl))) == 'mtCorrea')? 'active':'';?>" href="mtCorrea.php"><i class="fas fa-cogs"></i> Correa</a>
            </li>
            <li class="nav-item">
				<a class="nav-link" href="/"><i class="fas fa-sign-out-alt"></i> Salir</a>				  
            </li>
          </ul>
		  
        </div>
      </div>
    </nav>