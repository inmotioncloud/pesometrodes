<?php
require_once "config/class.conex.php" ;
require_once "config/helper.php" ;

class manifiestoModel{
   
	private $db;
	private $url;
	
	function manifiestoModel(){
		$this->db = new conexion();
		$this->helper = new helper();
		
	}	

    public function getData(){

		$query = "SELECT * FROM [dbo].[manifiesto_cab] ORDER BY [date] DESC;";

		$result =$this->db->db_query($query);
		$rows = array();
				
		while($row = $this->db->db_fetch_object($result)){			
		
			$row->id = (is_null($row->id))? "---":$row->id;
			$row->puerto_origen = (is_null($row->puerto_origen))? "---":$row->puerto_origen;
			$row->puerto_destino = (is_null($row->puerto_destino))? "---":$row->puerto_destino;
			$row->date = (is_null($row->date))? "---":date_format($row->date,'d-m-Y');	
			if($row->activo != 1){
				
				// Si hoy es la misma fecha que aparece en el manifiesto, sebe aparecer el boton de activar.
				$activar = ($row->date == date('d-m-Y'))? '<button type="button" class="btn btn-success btn-sm" onClick="openModal('.intval($row->id).',2)"><i class="fas fa-ship"></i> Activar</button> ':'';
				
				// Si la fecha es menor o igual a hoy
				$editar = ($this->db->comparar_fechas($row->date, date('d-m-Y')) == -1)? '':'<a href="'.$this->helper->url().$row->id.'" class="btn btn-primary btn-sm"><i class="far fa-edit"></i> Editar</a>';
				
				// Acciones
				$row->acciones = (is_null($row->id))? '': $activar.$editar.' '.'<button type="button" class="btn btn-primary btn-danger btn-sm" data-toggle="modal" data-target="#modalEliminar" onClick="openModal('.intval($row->id).',1)"><i class="far fa-trash-alt"></i> Eliminar</button>';
			} else { 
				// cuando esta activo
				$row->acciones = '<strong>Manifiesto Activo</strong>  <a href="'.$this->helper->url().$row->id.'" class="btn btn-primary btn-sm"><i class="far fa-eye"></i> Ver</a>';
			}
			array_push($rows ,$row);
		}
		
		return json_encode(array("data"=>$rows));
	}

	public function getCabManifiesto( $id = null){
		
		$query = "SELECT * FROM [dbo].[manifiesto_cab] WHERE [id] ='".$id."';";

		$result =$this->db->db_query($query);
		$rows = array();
				
		while($row = $this->db->db_fetch_object($result)){			
		
			$row->id = (is_null($row->id))? "---":$row->id;
			$row->puerto_origen = (is_null($row->puerto_origen))? "---":$row->puerto_origen;
			$row->puerto_destino = (is_null($row->puerto_destino))? "---":$row->puerto_destino;
			$row->date = (is_null($row->date))? "---":date_format($row->date,'d-m-Y');			
			$row->acciones = (is_null($row->id))? '': '<a href="'.$this->helper->url().$row->id.'" class="btn btn-primary btn-sm"><i class="far fa-plus-square"></i> Editar</a>'.' '.'<button type="button" class="btn btn-primary btn-danger btn-sm" data-toggle="modal" data-target="#modalEliminar" onClick="openModal('.intval($row->id).')"><i class="far fa-trash-alt"></i> Eliminar</button>';
			
			array_push($rows ,$row);
		}
		return $rows;

	}

	public function getDetManifiesto( $id = null){
		
		$query = "SELECT [manifiesto] , 
						[indice],
						CAST([hold] as Varchar) as hold, 
						CAST([tons] as VARCHAR) as tons, 						
						a.[material] AS material, 
						b.[min_vel] ,
						b.[max_vel] 
					FROM [dbo].[manifiesto_det] a 
					LEFT JOIN [dbo].[materiales] b ON a.[material]=CAST(b.[id] as VARCHAR)
					WHERE a.[manifiesto] = '".$id."'  
					ORDER BY [indice];";
		
		
		$result =$this->db->db_query($query);
		$rows = array();
		
		while($row = $this->db->db_fetch_object($result)){			
		
			$row->manifiesto = (is_null($row->manifiesto))? "---":$row->manifiesto;
			$row->indice = (is_null($row->indice))? "---":$row->indice;
			$row->hold = (is_null($row->hold))? "---":$row->hold;
			$row->tons = (is_null($row->tons))? "---":$row->tons;			
			$row->material = (is_null($row->material))? "---":$row->material;
			$row->min_vel = (is_null($row->min_vel))? "---":$row->min_vel;
			$row->max_vel = (is_null($row->max_vel))? "---":$row->max_vel;
			
			array_push($rows ,$row);
		}

		return $rows;

	}

	public function getBodegaMaterialManifiesto( $id = null){

		$query = "SELECT [manifiesto]
						,[bodega]
						,[mat_1]
						,[mat_2]
						,[mat_3]
					FROM [dbo].[bodega_material] 					
					WHERE [manifiesto] ='".$id."'";
		
		$result =$this->db->db_query($query);
		$rows = array();
		
		while($row = $this->db->db_fetch_object($result)){			
		
			$row->manifiesto = (is_null($row->manifiesto))? "---":$row->manifiesto;
			$row->bodega = (is_null($row->bodega))? "---":$row->bodega;
			$row->mat_1 = (is_null($row->mat_1))? "---":$row->mat_1;
			$row->mat_2 = (is_null($row->mat_2))? "---":$row->mat_2;			
			$row->mat_3 = (is_null($row->mat_3))? "---":$row->mat_3;			
			
			array_push($rows ,$row);
		}

		return $rows;	
	}

	public function setActivaManifiesto($idManifiesto){
		// Desactiva todos los manifiestros
		$query = "UPDATE [dbo].[manifiesto_cab] SET [activo] = 0 WHERE [activo] = 1;";
		$result =$this->db->db_query($query);		
		
		// Activa el manifiesto requerido
		$query = "UPDATE [dbo].[manifiesto_cab] SET [activo] = 1 WHERE [id] = ".intval($idManifiesto);
		$result =$this->db->db_query($query);
		
		// Desactiva todas las bodegas
		$query = "UPDATE [manifiesto_det] SET [activo] = 0 WHERE [activo] = 1;";
		$result =$this->db->db_query($query);
		
		// Activa primera bodega
		$query = "UPDATE [manifiesto_det] SET [activo] = 1 WHERE [manifiesto] = ".intval($idManifiesto)." AND [material] !=0 AND [indice] = ".$this->primerIndice($idManifiesto);
		$result =$this->db->db_query($query);
		
		
		if($result){
			return true;
		} else {
			return false;
		}
		
	}
	
	private function primerIndice($idManifiesto){
		$query = "SELECT TOP (1) indice FROM [manifiesto_det] WHERE [manifiesto] = ".intval($idManifiesto)." AND [material] !=0 ORDER BY [indice]";
		$result =$this->db->db_query($query);		
		$row = $this->db->db_fetch_object($result);
		return intval($row->indice);
	}

	public function guardarManifiesto($origen,$destino,$fecha){

		$qry = "INSERT INTO [dbo].[manifiesto_cab]
								([puerto_origen]
								,[puerto_destino]
								,[date])
						VALUES	('".$origen."',
						'".$destino."',
						'".$fecha."')";
	

		$result = $this->db->db_query_insert_id($qry);
		
		return $result;

	}

	public function guardarManifiestoMaterial( $manifiesto , $bodega , $mat1 , $mat2 , $mat3 ){

		$qry = "INSERT INTO [dbo].[bodega_material]
           ([manifiesto]
           ,[bodega]
           ,[mat_1]
           ,[mat_2]
           ,[mat_3])
    			 VALUES
           ('".$manifiesto."',
           	'".$bodega."',
           	'".$mat1."',
           	'".$mat2."',
           	'".$mat3."')";	

		if($this->db->db_query($qry)){
			return true;
		} else {
			return false;
		}

		

	}

	public function guardarManifiestoDetalle( $manifiesto , $indice , $bodega , $tons , $mat ){	

		$qry = "INSERT INTO [dbo].[manifiesto_det]
						([manifiesto]
						,[indice]
						,[hold]
						,[tons]
						,[material])
				VALUES
						(	'".$manifiesto."',
							".intval($indice).",
							".intval($bodega).",
							".intval($tons).",
							".$mat.");";
							
		if($this->db->db_query($qry)){
			return true;				
		} else {
			return false;
		}
	}

	public function deleteManifiesto($manifiesto){
		
		$this->deleteManifiestoDet($manifiesto);
		$this->deleteManifiestoCab($manifiesto);
		$this->deleteBodegaMaterial($manifiesto);
		$this->deleteManifiestoAlertas($manifiesto);
		return true;
	}

	private function deleteManifiestoDet($manifiesto){
		$qry="DELETE 
				FROM [dbo].[manifiesto_det]
				WHERE [manifiesto] ='".$manifiesto."'";
		
		$this->db->db_query($qry);
		return true;
	}

	private function deleteManifiestoCab($manifiesto){
		$qry="DELETE 
				FROM [dbo].[manifiesto_cab]
			  WHERE [id] ='".$manifiesto."'";

		$this->db->db_query($qry);
		return true;
	}

	
	private function deleteBodegaMaterial($manifiesto){
		$qry = "DELETE 
					FROM [dbo].[bodega_material]
				  WHERE [manifiesto] ='".$manifiesto."'"; 		
		$this->db->db_query($qry);
		return true;
	}
	private function deleteManifiestoAlertas($manifiesto){
		$qry="DELETE 
				FROM [dbo].[alerta_pesometro]
			  WHERE [manifiesto] ='".$manifiesto."'";

		$this->db->db_query($qry);
		return true;
	}
	
	public function updateManifiesto($manifiesto, $origen,$destino,$fecha){
		$qry = "UPDATE [dbo].[manifiesto_cab]
				SET [puerto_origen] = '".$origen."',
					[puerto_destino] = '".$destino."',
					[date] = '".$fecha."'
				WHERE [id] = '".$manifiesto."'";
	
		if($this->db->db_query($qry)){
			return true;
		} else {
			return false;
		}
		
	}

	public function updateManifiestoMaterial( $manifiesto , $bodega , $mat1 , $mat2 , $mat3){

		$qry = "UPDATE [dbo].[bodega_material]           
           SET 	[mat_1] = '".$mat1."' ,
				[mat_2] = '".$mat2."',
				[mat_3] = '".$mat3."'
			WHERE [manifiesto] = '".$manifiesto."'
				AND [bodega] = '".$bodega."'";           
           	
		if($this->db->db_query($qry)){
			return true;
		} else {
			return false;
		}
	}

	public function updateManifiestoDetalle( $manifiesto , $indice , $bodega , $tons , $mat ){

		if( $manifiesto == '' )	{ $manifiesto = 0 ; }
		if( $indice == '') { $indice = 0 ; }
		if( $bodega == '') { $bodega = 0 ; }
		if( $tons == '' ) { $tons = 0; } 
		if( $mat == '' ) { $mat =0 ;}

		if( $manifiesto != '' && $indice != '' &&  $bodega != '' && $tons != '' && $mat != '' )	{
			
			$qry = "UPDATE [dbo].[manifiesto_det]
					SET [hold] = '".$bodega."',
						[tons] = '".$tons."',
						[material] = '".$mat."'
					WHERE [manifiesto] = '".$manifiesto."' AND [indice] = '".$indice."' " ;

			
			if($this->db->db_query($qry)){
				return true;
			} else {
				return false;
			}
			
		} else {
			return false;
		}
	}
	
	public function esActivoManifiesto($manifiesto){
		
		$query = "SELECT COUNT(id) AS 'cant' FROM [dbo].[manifiesto_cab] WHERE [id] = '".$manifiesto."' AND [activo] = 1;";
		
		$result = $this->db->db_query($query);		
		$row = $this->db->db_fetch_object($result);
		
		return $row->cant;
		
	}
  
}


?>
