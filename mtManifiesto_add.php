<?php
require_once "controller/manifiestoController.php";

$objMateriales= new manifiestoController;
$materiales = $objMateriales->getDataMateriales();
$esActivo = false;
if(isset($_GET['id']) && $_GET['id'] != '') {
	$manifObj = new manifiestoController();
	
	$manif = intval($_GET['id']);
	$esActivo = $manifObj->esActivoManifiesto($manif);	
	
	$manifCab = $manifObj->getcabmanif($manif);
	$manifDet = $manifObj->getdetmanif($manif);
	$manifBod = $manifObj->getbodmanif($manif);

	foreach( $manifCab as $cab ){
		$id = $cab->id;
		$puerto_origen = $cab->puerto_origen;
		$puerto_destino = $cab->puerto_destino;
		if(isset($cab->date)){
			$dd = date_create( $cab->date);
			$fecha = date_format($dd, 'Y-m-d');			
		}
	}
}
?>
<!DOCTYPE html>

<html lang="en">

  <head>
  	<?php require_once "meta.php";?> 
  	<style>
		.nav-pills .nav-link.active, .nav-pills .show > .nav-link {
			color: #fff !important;
			background-color: #ccc;		
		}
	</style>	
  </head>
  
  <body>

	<?php require_once "menu.php";?>

    <!-- Page Content -->
    <div class="container">
	   <!-- Page Heading/Breadcrumbs -->
      <h1 class="mt-4 mb-3"><i class="far fa-list-alt"></i> TW SHIP MANAGMENT PVT. LTD.</h1>

      <ol class="breadcrumb">
        <li class="breadcrumb-item">Home</li>
        <li class="breadcrumb-item active">Manifiesto</li>
		<li class="breadcrumb-item active"><?=(isset($_GET['id']) && $_GET['id']!=0)? 'Modificación de Manifiesto':'Agregar Nuevo Manifiesto';?></li>
      </ol>
	  
		<div class="row">
	
		<div class="col-md-12">
			<div class="card mb-4">				
				<div class="card-body">

					<form class="form-horizontal" id="form_cab" name="form_cab">
						<input type="hidden" id="manifiestoId" value="<?=(isset($_GET['id']) && $_GET['id']!=0)? intval($_GET['id']):'0';?>" readonly=""/>			
						
						<div class="row">

							<div class="form-group col-md-4">															
								<label for="exampleFormControlInput1">Puerto de origen</label>
								<input type="text" class="form-control" id="puerto_origen" name="puerto_origen" value="<?=(isset($puerto_origen))? $puerto_origen:'';?>">								
							</div>
							<div class="form-group col-md-4">
								<label for="exampleFormControlInput1">Puerto de Destino</label>
								<input type="text" class="form-control" id="puerto_destino" name="puerto_destino" value="<?=(isset($puerto_destino))? $puerto_destino:'';?>">								
							</div>
							<div class="form-group col-md-4">
								<label for="exampleFormControlInput1">Fecha Inicio</label>
								<input type="date" class="form-control" id="fecha_inicio" name="fecha_inicio" maxlength="10" placeholder="dd-mm-aaaa" value="<?=(isset($fecha))? $fecha:'';?>" min="<?=date("Y-m-d")?>">								
							</div>
							
						</div>
					</form>
				</div>
			</div>
		</div>
		
<?php
/* ordenamos el array para que no se presenten problemas con la aprición de las bodegas
		verificamos que existe un manifiesto con datos para editar o crear un nuevo manifiesto
*/

/* Despliegue de bodegas HOLD */
 
	for($i=1; $i<=5; $i++){	
	?>
		<div class="col col<?=$i;?>">
			<div class="card">	
				<h3 class="card-header text-center">HOLD <?=$i?></h3>			
				<div class="card-body">					
					<form class="form" id="FORM_HOLD_<?=$i?>">						
					<?php 	
						for($j=1; $j<=3; $j++){
							echo '<select class="custom-select custom-select-sm select_mat_tabla select_hold_'.$i.'" id="select_hold_'.$i.'_'.$j.'" name="select_hold_'.$i.'_'.$j.'" style="margin-bottom:8px">';
							echo '<option selected value="0">-- Seleccione --</option>';						
								foreach($materiales as $list){
									$selected = (@$manifBod[($i-1)]->{"mat_".$j} == $list->id)? 'selected':'';										
									echo '<option value="'. $list->id.'" '.$selected.'>'.$list->name.'</option>';									
								}									
							echo "</select>";									
						}
					?>					
					</form>
				</div>
			</div>			
		</div>
<?php
	} // fin for
?>	
<br><br>
<br><br>
		<!-- Content Row -->
		<div class="col-md-12">
			<div class="card">
				<div class="card-body">
					
					<table class="table table-striped">
						<thead>
							<tr class="bg-success">
							<th scope="col">Load</th>
							<th scope="col" style="width:150px">Hold</th>
							<th scope="col" style="width:400px">Prod</th>
							<th scope="col" style="width:150px">Tones</th>
							<th scope="col" class="text-center">Vel. Mín.</th>
							<th scope="col" class="text-center">Vel. Máx.</th>							  
							</tr>
						</thead>
						<tbody>							
							<form class="form" id="FORM_TABLA">
									<?php		
									/* Despliegue orden de carga */
									
									$TOTAL = 0;
									for($z=1; $z<=10; $z++){
									
										$HOLD = @round(@$manifDet[($z-1)]->hold);
										$MATE = @round(@$manifDet[($z-1)]->material);
										$TONS = @round(@$manifDet[($z-1)]->tons);
										$MIN = @round(@$manifDet[($z-1)]->min_vel);
										$MAX = @round(@$manifDet[($z-1)]->max_vel);
										
										$TOTAL += @round(@$manifDet[($z-1)]->tons);
									?>
										<tr>
										<th scope="row"><?=$z?></th>
										<td>
											<select class="custom-select select_hold_tabla seleccionBodegaBarco" id="select_hold_<?=$z?>" name="select_hold_<?=$z?>">
												<option value="0">-- Hold --</option>
												<?php
												for( $i=1 ; $i<=5 ; $i++ ) {
													$selected = ($HOLD == $i)? 'selected':'';
													echo '<option value="'.$i.'" '.$selected.'>HOLD '.$i.'</option>';
												}																				
												?>
											</select>
											
										</td>
										<td>
											<select class="custom-select select_mat_tabla" id="select_mat_<?=$z?>" name="select_mat_<?=$z?>" <?=(isset($materiales) && intval($materiales)>0 && $HOLD > 0)? '':'disabled'?>>
												<option selected value="0">-- Seleccione --</option>												
												<?php
												if(isset($materiales) && intval($materiales)>0){
													foreach($materiales as $list) {														
														if(@$manifBod[$HOLD-1]->mat_1 == $list->id || @$manifBod[$HOLD-1]->mat_2 == $list->id || @$manifBod[$HOLD-1]->mat_3 == $list->id){
															$selected = ($MATE == $list->id)? 'selected':'';
															echo '<option value="'.$list->id.'" '.$selected.'>'.$list->name.'</option>';										
														}
													}
												}
												?>
											</select>
										</td>
										<td>
											<div class="form-group">									
												<input type="number" class="form-control tons_text" min="0" max="9999" id="<?="tons_".$z;?>" name="<?="tons_".$z;?>" value="<?=($TONS>0)? $TONS:'0'?>" <?=(isset($materiales) && intval($materiales)>0 && $HOLD > 0)? '':'disabled'?>>
											</div>
										</td>
										
										<td class="text-center" id="min_<?=$z?>"><?=($MIN>0)?$MIN:''?></td>
										<td class="text-center" id="max_<?=$z?>"><?=($MAX>0)?$MAX:''?></td>
										
										</tr>
										
									<?php							
									} // fin for							
									?>
																			
									<tr class="bg-success">
									<td></td>
									<td></td>
									<th scope="row">Total</th>												
									<td class="text-center" id="total_tons" style="font-weight:bold;font-size:18px"><?=$TOTAL?></td>
									<td></td>
									<td></td>							 
									</tr>
							</form>
						</tbody>
					</table>
					

				</div>
			</div>
		</div>
	
		<div class="col-md-12">
			<div class="card">
				<div class="card-body text-center">
					<?php if($esActivo){ ?>					
					<button id="btnCancelar" name="btnCancelar" type="button" class="btn btn-warning btn-lg"><i class="fas fa-arrow-left"></i> Volver</button>
					<?php } else { ?>
					<button id="btnGuardarManifiesto" name="btnGuardarManifiesto" type="button" class="btn btn-success btn-lg"><i class="far fa-save"></i> Guardar</button>
					<button id="btnCancelar" name="btnCancelar" type="button" class="btn btn-danger btn-lg"><i class="fas fa-ban"></i> Cancelar</button>
					<?php } ?>
				</div>
			</div>
		</div>
		<div id="mje"></div>
		
      <!--</div>-->

    <!--</div>-->

		</div>
		<!-- /.row -->

    </div>
    <!-- /.container -->

	<?php require_once "footer.php";?>

	<?php require_once "js.php";?>
	<script type="text/javascript">	
	
		var materialesArray = <?=json_encode($materiales)?>;
		var toneladas = 0;
  
		$(document).ready(function(){
			
			<?php if($esActivo){ ?>
				$("input, select").prop('disabled', true);
			<?php } ?>			
			
			$(".seleccionBodegaBarco").on("change", function(){
				// obtiene indice
				var str = $(this).attr('id');
				var indice = str.split("select_hold_");	
				
				if($(this).val()==0){
					// cuando seleccionan una bodega con el indice cero, vuelve los valores por defecto.
					$("#select_mat_"+indice[1]).prop('selectedIndex',0);
					$("#select_mat_"+indice[1]).attr('disabled','disabled');				
					$("#tons_"+indice[1]).attr('disabled','disabled');		
					$("#tons_"+indice[1]).val(0);
					$("#min_"+indice[1]).html('');
					$("#max_"+indice[1]).html('');				
					
				} else {				
					// Selecciona una bodega
					
					// Obtiene valores de los select
					var arr = $("#FORM_HOLD_"+$(this).val()).serializeArray();
					
					// valida que contenga productos los hold de arriba
					if(arr[0]['value']==0 && arr[1]['value']==0 && arr[2]['value']==0){
						// si no tiene productos 
						$("#select_mat_"+indice[1]).prop('selectedIndex',0);
						$("#select_mat_"+indice[1]).attr('disabled','disabled');				
						$("#tons_"+indice[1]).attr('disabled','disabled');		
						$("#tons_"+indice[1]).val(0);
						$("#min_"+indice[1]).html('');
						$("#max_"+indice[1]).html('');
						bootbox.alert({
							message: '<div class="alert alert-warning"><strong>Advertencia!</strong><br> HOLD '+$(this).val()+' no contiene productos asociados.</div>',
							callback: function () {
								$("#select_hold_"+indice[1]).prop('selectedIndex',0);
							}
						});
						
						return false;
						
					} else {
						// cuando contiene materiales, activa la seleccion de materiales 
						$("#select_mat_"+indice[1]).removeAttr('disabled');
						$("#min_"+indice[1]).html('');
						$("#max_"+indice[1]).html('');
					}
					
					// llena select con productos de la bodega seleccionada			
					$('#select_mat_'+indice[1]).html("<option selected value='0'>-- Seleccione --</option>");			
					$.each(materialesArray, function( index, value ) {
						if(arr[0]['value'] == value.id || arr[1]['value'] == value.id || arr[2]['value'] == value.id){
							$('#select_mat_'+indice[1]).append('<option value="'+ value.id + '">' +  value.name + '</option>');
						}
					});
					
					// cuando se selecciona un material dentro del cuadro de indice
					$('#select_mat_'+indice[1]).on("change", function(){
						var idMat = $(this).val();						
						if(idMat==0){
							// si no seleccioan nada, limpia y deshabilita los demas elementos
							$("#min_"+indice[1]).html('');
							$("#max_"+indice[1]).html('');
							$("#tons_"+indice[1]).removeAttr('disabled').val(0);	
							$("#tons_"+indice[1]).attr('disabled','disabled');						
						} else {				
							// al seleccionar un material, activa y habilita los demas elementos.
							$("#tons_"+indice[1]).removeAttr('disabled').val(0);
							$("#tons_"+indice[1]).focus();
							$.each(materialesArray, function( index, value ) {
								if(idMat == value.id){
									$("#min_"+indice[1]).html(value.min_vel);
									$("#max_"+indice[1]).html(value.max_vel);
								}
							});
						}
					});
					
				} // fin if
			});			
			
			// realiza sumarotia de las toneladas.
			$(".tons_text").change(function() {
				toneladas = 0;
				$(".tons_text").each(function( key, value ) {					
					if(!$.isNumeric($(this).val())){						
						toneladas = "Valor erróneo";
						return false;
					} else {
						toneladas += parseInt($(this).val());						
					}
				});	
				$('#total_tons').html(toneladas) ;				
			});
			
			// boton cancelar
			$("#btnCancelar").on("click", function(){
				$(location).attr('href','mtManifiesto.php');
			});
			
			<?php if(!$esActivo){ ?>
			// guarda manifiesto
			$("#btnGuardarManifiesto").on("click", function(){
							
				if(validarSiNumero($("#puerto_origen").val())==2 || validarSiNumero($("#puerto_destino").val())==2){
					bootbox.alert('<div class="alert alert-warning"><strong>Advertencia!</strong><br>Los puertos de origen o destino deben ser letras.</div>');
					return (false);
				}
				
				if($('#fecha_inicio').val() == ''){
					bootbox.alert('<div class="alert alert-warning"><strong>Advertencia!</strong><br>ingrese una fecha igual o mayor a la actual.</div>');
					return (false);
				}				
				
				if(!isValidDate($("#fecha_inicio").val())){
					bootbox.alert('<div class="alert alert-warning"><strong>Advertencia!</strong><br>La fecha ingresada no es válida.</div>');
					return (false);
				}
				
				var filaError=0;
				for (k=1;k<=10;k++){
					if(validarSiNumero($('#tons_' + k).val()) == 1 || $('#tons_' + k).val() == '' ){
						filaError=1;
					}else{
						filaError=0;
					}
				}
				
				if(filaError>0){
					bootbox.alert('<div class="alert alert-warning"><strong>Advertencia!</strong><br>Los campos de tonelaje deben ser numericos!</div>');
					return (false);
				}

				var filaError=0;
				
				for (k=1;k<=10;k++){
					if(validaHoldMateriales(k)==1){
						bootbox.alert('<div class="alert alert-warning"><strong>Advertencia!</strong><br>Debe ingresar todos los campos requeridos para la fila.!</div>');
						return (false);
					}
			
				}
				
				var arr_cab = $("#form_cab").serialize();	
				
				var arr_det_hold1 = $("#FORM_HOLD_1").serialize();	
				var arr_det_hold2 = $("#FORM_HOLD_2").serialize();	
				var arr_det_hold3 = $("#FORM_HOLD_3").serialize();	
				var arr_det_hold4 = $("#FORM_HOLD_4").serialize();	
				var arr_det_hold5 = $("#FORM_HOLD_5").serialize();	

				var arr_tabla = $("#FORM_TABLA").serialize();	
				
				if($("#manifiestoId").val() == 0){
					// crear manifiesto
					var cadena = "&opAcc=2&" + arr_cab + "&" + arr_det_hold1 + "&" + arr_det_hold2 + "&" + arr_det_hold3 + "&" + arr_det_hold4 + "&" + arr_det_hold5  + "&" + arr_tabla;
				} else {
					// actualizar manifiesto
					var manifiestoId = $("#manifiestoId").val();
					var cadena = "&opAcc=5&manifiestoId=" + manifiestoId + "&" + arr_cab + "&" + arr_det_hold1 + "&" + arr_det_hold2 + "&" + arr_det_hold3 + "&" + arr_det_hold4 + "&" + arr_det_hold5  + "&" + arr_tabla ;
				}
				
				$.ajax({
					url: "manifiesto_ajax.php",
					type: "POST",			
					data:  cadena,
					beforeSend : function (){
						HoldOn.open({theme:'sk-rect', message:"<h4>Cargando...</h4>"});
					},
					success: function(response){
						
						var data = JSON.parse(response);
					
						if(data.result==1){
							
							bootbox.alert({
								message: '<div class="alert alert-success"><strong>OK!</strong><br>'+data.mje+'</div>',
								callback: function () {
									$(location).attr('href',data.url);							
								}
							});	
							
							HoldOn.close();
						} else {
							bootbox.alert('<div class="alert alert-danger"><strong>Error!</strong><br>'+data.mje+'</div>');
							HoldOn.close();
						}				
					}
				});
			});
			<?php } ?>
		
		}); // fin ready	
	
		function validarSiNumero(numero){
			if (!/^([0-9])*$/.test(numero)){
				//letra
				return 1;      		
			}else{
				//numero
				return 2;
			}		
		}

		function validaHoldMateriales(fila){				
			var hold = $('#select_hold_' + fila).val();
			var mate = $('#select_mat_' + fila).val(); 
			var tons = $('#tons_' + fila).val(); 		
			
			if( hold > 0 && mate > 0 && tons >0 ){
				return (hold + "    " + mate + "      " + tons); 
			}else{
				return 0;
			}
		}
		
	</script>
	<script>
	
		function isValidDate(dateString){
		
			// revisar el patrón
			if(!/^\d{4}\-\d{1,2}\-\d{1,2}$/.test(dateString))
				return false;

			// convertir los numeros a enteros
			
			var parts = dateString.split("-");
			
			var year = parseInt(parts[0], 10);
			var month = parseInt(parts[1], 10);
			var day =  parseInt(parts[2], 10);
		
			// Revisar los rangos de año y mes
			if( (year < 1000) || (year > 3000) || (month == 0) || (month > 12) )
				return false;

			var monthLength = [ 31, 28, 31, 30, 31, 30, 31, 31, 30, 31, 30, 31 ];

			// Ajustar para los años bisiestos
			if(year % 400 == 0 || (year % 100 != 0 && year % 4 == 0))
				monthLength[1] = 29;

			// Revisar el rango del dia
			return day > 0 && day <= monthLength[month - 1];
			
		};
	</script>
  </body>
</html>