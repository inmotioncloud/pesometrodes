<?php
/**----------------------------------------------------------------------------------
* Microsoft Developer & Platform Evangelism
*
* Copyright (c) Microsoft Corporation. All rights reserved.
*
* THIS CODE AND INFORMATION ARE PROVIDED "AS IS" WITHOUT WARRANTY OF ANY KIND, 
* EITHER EXPRESSED OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE IMPLIED WARRANTIES 
* OF MERCHANTABILITY AND/OR FITNESS FOR A PARTICULAR PURPOSE.
*----------------------------------------------------------------------------------
* The example companies, organizations, products, domain names,
* e-mail addresses, logos, people, places, and events depicted
* herein are fictitious.  No association with any real company,
* organization, product, domain name, email address, logo, person,
* places, or events is intended or should be inferred.
*----------------------------------------------------------------------------------
**/

class Config
{
	const ACCOUNT_NAME = 'storagepesometrodes';
    const ACCOUNT_KEY = '6hg7EIfhRtqQbr6cOUZLHlrgDqy232xPXArtpX9PqZFohsPKWmbCGqbbGP+X+nBw+1rsaM1xZe7vs23OrvtMqA==';
    const IS_EMULATED = false;
	
    /*
	const ACCOUNT_NAME = 'pesometro';
    const ACCOUNT_KEY = 'c+daTGgTtdjfwnbknsZRYE2tKKS8MieTdm7VW24oLJ6azlbyb2m/YSOKISmyH1ZXDBRhhL1x8/khj4pXkKaYIg==';
    const IS_EMULATED = false;
	*/
    public static function getConnectionString(){
		if(Config::IS_EMULATED){
			return "UseDevelopmentStorage=true";
		} else {
			return "DefaultEndpointsProtocol=https;AccountName=" . Config::ACCOUNT_NAME . ";AccountKey=" . Config::ACCOUNT_KEY;
		}
    }
}
?>