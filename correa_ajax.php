<?php
require_once "controller/correaController.php";
require_once "controller/class.inputfilter_clean.php";

$sanitizer = new InputFilter();
foreach($_POST as $key=>$val){
	$arr[$key] =  $sanitizer->process($val);
}

$list = new correaController();

switch(intval($arr['opAcc'])){
	case 1:		
		// obtiene listado de datos de correas.
		echo $list->getData();
		break;
	case 2:
		// resetear uso de correa
		$list->resetCorrea();
		break;
	default:
		exit();
		break;
}

?>