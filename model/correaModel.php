<?PHP

require_once "config/class.conex.php";

class correaModel{
	private $db;	
	public $txtMotivo;
	
	function correaModel(){
		$this->db = new conexion();
	}

	public function getData(){
		// obtiene datos del uso de correas
		$query = "SELECT * FROM [dbo].[alerta_correa]";
		$result = $this->db->db_query($query);
		$rows = array();
				
		while($row = $this->db->db_fetch_object($result)){			
						
			$row->nombre_correa = (is_null($row->nombre_correa))? "---":$row->nombre_correa;
			
			$row->hrs_uso_correa = number_format(intval($row->hrs_uso_correa), 0, ',', '.');
			
			$row->fch_alerta_uno = (is_null($row->fch_alerta_uno)? "---":date_format($row->fch_alerta_uno, 'Y-m-d H:i:s'));
			$row->fch_alerta_dos = (is_null($row->fch_alerta_dos)? "---":date_format($row->fch_alerta_dos, 'Y-m-d H:i:s'));
			$row->fch_alerta_tres = (is_null($row->fch_alerta_tres)? "---":date_format($row->fch_alerta_tres, 'Y-m-d H:i:s'));
			$row->fch_alerta_cuatro = (is_null($row->fch_alerta_cuatro)? "---":date_format($row->fch_alerta_cuatro, 'Y-m-d H:i:s'));
			array_push($rows ,$row);
		}		
		
		return json_encode($rows);
	}	
	
	public function resetCorrea(){
		
		// resetea registro de uso de correa.		
		$qry = "UPDATE [dbo].[alerta_correa] SET [hrs_uso_correa] = 0, [fch_alerta_uno] = NULL, [fch_alerta_dos] = NULL, [fch_alerta_tres] = NULL, [fch_alerta_cuatro] = NULL;";
		$result = $this->db->db_query($qry);
		
		if( $result === false ) {
			return false;
		} else {
			return true;
		}		
	}

}


?>