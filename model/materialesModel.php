<?php
require_once "config/class.conex.php" ;
require_once "config/helper.php" ;

class materialesModel{
    
    private $db;	
	
	function materialesModel(){
		$this->db = new conexion();
	}	

    public function getMateriales(){

		$query = " SELECT [id]
                        ,[name]
                        ,[min_vel]
                        ,[max_vel]
                   FROM 
                        [dbo].[materiales]";

        $result =$this->db->db_query($query);
        $rows = array();

        while( $row = $this->db->db_fetch_object($result) ){			
		
			$row->id = (is_null($row->id))? "---":$row->id;
			$row->name = (is_null($row->name))? "---":$row->name;
			$row->min_vel = (is_null($row->min_vel))? "---":$row->min_vel;
			$row->max_vel = (is_null($row->max_vel))? "---":$row->max_vel;
            
            array_push($rows ,$row);
        }
        return $rows;
    }
    
    public function getDetalleMaterial($id = null)
    {
        $query = " SELECT [id]
                        ,[name]
                        ,[min_vel]
                        ,[max_vel]
                   FROM 
                        [dbo].[materiales]
                    WHERE [ID] = ".$id.";";

        $result =$this->db->db_query($query);
        $rows = array();

        while( $row = $this->db->db_fetch_object($result) ){			
		
			$row->id = (is_null($row->id))? "---":$row->id;
			$row->name = (is_null($row->name))? "---":$row->name;
			$row->min_vel = (is_null($row->min_vel))? "---":$row->min_vel;
			$row->max_vel = (is_null($row->max_vel))? "---":$row->max_vel;
            
            array_push($rows ,$row);
        }
        return $rows;
    
    }

    public function getDetalleMaterialId($ids = null)
    {
        
        $query = " SELECT [id]
                        ,[name]
                        ,[min_vel]
                        ,[max_vel]
                   FROM 
                        [dbo].[materiales]
                    WHERE [ID] IN (".$ids.");";

        $result =$this->db->db_query($query);
        $rows = array();

        while( $row = $this->db->db_fetch_object($result) ){			
		
			$row->id = (is_null($row->id))? "---":$row->id;
			$row->name = (is_null($row->name))? "---":$row->name;
			$row->min_vel = (is_null($row->min_vel))? "---":$row->min_vel;
			$row->max_vel = (is_null($row->max_vel))? "---":$row->max_vel;
            
            array_push($rows ,$row);
        }
        return $rows;
    
    }

}